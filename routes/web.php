<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['session.login'])->group(function () {
    Route::get('/', function () {
        return view('dashboard.index');
    })->name('dashboard');

    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login', 'AuthController@postLogin')->name('post.login');

    Route::get('/register', 'AuthController@register')->name('register');
    Route::post('/register', 'AuthController@postRegister')->name('post.register');

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/profile-edit', 'ProfileController@edit')->name('profile.edit');
    Route::post('/profile-update', 'ProfileController@update')->name('profile.update');
});
