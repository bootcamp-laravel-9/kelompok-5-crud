<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'Mikael Rizki', 'username' => 'mikaelrizki', 'photo_url' => 'https://e1.pxfuel.com/desktop-wallpaper/903/679/desktop-wallpaper-97-aesthetic-best-profile-pic-for-instagram-for-boy-instagram-dp-boys.jpg', 'address' => 'Jl. Mangga','email' => 'mikaelrizki@gmail.com', 'password' => bcrypt('12345678')],
            ['name' => 'Novianto', 'username' => 'allui', 'photo_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgaPXNJJZb4KK8JafCmqnlvMMaULiFMTIMEA&usqp=CAU', 'address' => 'Jl. Durian', 'email' => 'novianto@gmail.com', 'password' => bcrypt('12345678')],
        ];

        DB::beginTransaction();

        foreach ($users as $user) {
            User::firstOrCreate($user);
        }

        DB::commit();
    }
}
