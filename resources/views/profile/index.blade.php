@extends('layout/main')
@section('heading', 'Profile')
@section('menu-profile', 'active')
@section('content')

    @if (request()->segment(1) == 'profile')
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h3 class="card-title text-white">Profile Information</h3>
                    </div>
                    <div class="card-body box-profile mt-2">
                        <div class="text-center mb-4">
                            <img class="profile-user-img img-fluid rounded-circle" src="/images/{{ $user->photo_url ?? 'default.jpg' }}"
                                alt="User profile picture" style="object-fit: cover; width: 200px; height: 200px;">
                        </div>

                        <h3 class="profile-username text-center"><b>{{ $user->name }}</b></h3>

                        <p class="text-muted text-center -mt-4">{{ '@' . $user->username }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h3 class="card-title text-white">Personal Information</h3>
                    </div>
                    <div class="card-body box-profile">
                        <div class="form-group row mt-2">
                            <label for="nama" class="col-sm-2 col-form-label"><b>Nama</b></label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control" id="text" placeholder="Nama"
                                    value="{{ $user->name }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-sm-2 col-form-label"><b>Username</b></label>
                            <div class="col-sm-10">
                                <input type="text" name="username" class="form-control" id="username"
                                    placeholder="Username" value="{{ $user->username }}" readonly>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label "><b>Email</b></label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                                    value="{{ $user->email }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-sm-2 col-form-label"><b>Alamat</b></label>
                            <div class="col-sm-10">
                                <input type="text" name="alamat" class="form-control" id="alamat"
                                    placeholder="Alamat" value="{{ $user->address }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-dark">
                        <h3 class="card-title text-white">Action</h3>
                    </div>
                    <div class="card-body box-profile">
                        <a href="{{ url('/profile-edit') }}" class="btn btn-warning">
                            <i class="fas fa-edit mr-2"></i>Edit Profile
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @elseif(request()->segment(1) == 'profile-edit')
        <form action="{{ url('/profile-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-header bg-dark">
                    <h3 class="card-title text-white">Edit Profile</h3>
                </div>
                <div class="card-body box-profile">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="form-group row">
                        <label for="name" class="col-sm-1 col-form-label"><b>Nama</b></label>
                        <div class="col-sm-11">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                                value="{{ $user->name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-1 col-form-label "><b>Email</b></label>
                        <div class="col-sm-11">
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                                value="{{ $user->email }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="username" class="col-sm-1 col-form-label"><b>Username</b></label>
                        <div class="col-sm-11">
                            <input type="text" name="username" class="form-control" id="username" placeholder="Username"
                                value="{{ $user->username }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-sm-1 col-form-label"><b>Alamat</b></label>
                        <div class="col-sm-11">
                            <input type="text" name="address" class="form-control" id="address"
                                placeholder="Alamat" value="{{ $user->address }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="photourl" class="col-sm-1 col-form-label"><b>Foto</b></label>
                        <div class="col-sm-11">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="photourl" name="photourl">
                                <label class="custom-file-label" for="photourl">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-1 col-form-label"><b>Password</b></label>
                        <div class="col-sm-11">
                            <input type="text" name="password" class="form-control" id="password"
                                placeholder="Left it Blank if you don't want to update the Password">
                        </div>
                    </div>

                    <a href="{{ url('/profile') }}" class="btn btn-dark mr-2">
                        <i class="fas fa-chevron-left mr-2"></i>Back
                    </a>
                    <button type="submit" class="btn btn-primary mr-2">
                        <i class="fas fa-save mr-2"></i>Save Changes
                    </button>
                    <button type="reset" class="btn btn-danger">
                        <i class="fas fa-sync mr-2"></i>Reset Changes
                    </button>
                </div>
        </form>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#photourl').on('change', function() {
                    //get the file name
                    var fileName = $(this).val().split('\\').pop();
                    //replace the "Choose a file" label
                    $(this).next('.custom-file-label').html(fileName);
                })
            });
        </script>
    @endif
@endsection
