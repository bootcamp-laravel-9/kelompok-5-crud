<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Auth - @yield('title', 'Auth')</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/styles.min.css') }}" />
</head>

<body>
    @yield('content')

    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
