<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div style="width: 80%;" class="mx-auto mt-2">
        <img src="{{ asset('assets') }}/berijalan.png" alt="Berijalan Logo" class="w-100">
    </div>
    <div class="user-panel mt-3 mx-2 d-flex">
    </div>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/images/{{ Session::get('user')->photo_url ?? 'default.jpg' }}" class="img-circle elevation-2" alt="User Image" style="border-radius: 50%; object-fit: cover; width: 36px; height: 36px;">
            </div>
            <div class="info">
                <a href="{{ url('/profile') }}" class="d-block text-uppercase">
                    {{ Session::get('user')->name }}
                </a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                    aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link @yield('menu-dashboard')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('profile') }}" class="nav-link @yield('menu-profile')">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profile
                        </p>
                    </a>
                </li>

                {{-- <li class="nav-item">
                    <a href="{{ route('member.index') }}" class="nav-link @yield('menu-member')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Member
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('biodata') }}" class="nav-link @yield('menu-biodata')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Curiculum Vitae
                        </p>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
