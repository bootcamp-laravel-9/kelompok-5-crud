<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'password' => 'required'
        // ]);

        $validated = $request->validate([
            // name must be required, max 24 characters, and no symbol or character
            'name' => 'required|max:24|regex:/^[a-zA-Z\s]*$/',
            // 'name' => 'required|max:24',
            'email' => 'required|max:50|email|unique:users,email',
            'password' => 'required',
            // username must be required, max 24 characters, and unique and nospaces
            'username' => 'required|max:24|unique:users,username|regex:/^\S*$/',
            'confirmpassword' => 'required|same:password'
        ]);

        if (!$validated) {
            return redirect()->route('register')->withInput();
        }


        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
            'username' => $validated['username']
        ]);

        return redirect()->route('login');
    }

    public function postLogin(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return redirect()->route('login')->with('error', 'Email/Password salah');
        }


        // $isValidPassword = password_verify($request->password, $user->password);
        $isValidPassword = Hash::check($request->password, $user->password);


        if (!$isValidPassword) {
            return redirect()->route('login')->with('error', 'Email/Password salah');
        }

        $token = $user->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('LoginSession', $token);
        $request->session()->put('user', $user);

        return redirect()->route('dashboard');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // $request->session()->forget('LoginSession');
        $request->session()->flush();
        return redirect()->route('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
