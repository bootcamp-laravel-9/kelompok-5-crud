<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(session('user')->id);
        return view('profile.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = User::find(session('user')->id);
        return view('profile.index', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // dd($request->all());

        // Validate the request
        $request->validate([
            'id' => 'required',
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'photourl' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);

        // Find the member by ID
        $updateUser = $user->find($request->id);

        if ($request->photourl == null) {
            $imageName = $updateUser->photo_url;
        } else {
            // get image file
            $image = $request->file('photourl');
            $imageName = time() . '.' . $image->extension();
            $image->move(public_path('images'), $imageName);

            // remove old image
            $oldImage = $updateUser->photo_url;
            if ($oldImage && file_exists(public_path('images/' . $oldImage))) {
                unlink(public_path('images/' . $oldImage));
            }
        }

        // Update member fields
        $updateUser->id = $request->id;
        $updateUser->name = $request->name;
        $updateUser->username = $request->username;
        $updateUser->photo_url = $imageName;
        $updateUser->address = $request->address;
        $updateUser->email = $request->email;
        if ($request->password)
            $updateUser->password = bcrypt($request->password);        

        // Redirect back or to another page after update
        if ($updateUser->save()) {
            session(['user' => $updateUser]);
            return redirect('/profile');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
